#ifndef _INIT_SHERWOOD_H_
#define _INIT_SHERWOOD_H_

#include "./sherwood_types.h"
#include <hdf5.h>

hid_t init_sherwood(char *, sherwood_t *);

#endif /* _INIT_SHERWOOD_H_ */
