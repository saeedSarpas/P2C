module libsherwood
   use hdf5
   use sherwood_types
   implicit none

   interface
      function init_sherwood_i(fpath, sherwood) bind(C, name="init_sherwood")
         use iso_c_binding
         implicit none
         character(len=1, kind=c_char), dimension(*), intent(in) :: fpath
         type(c_ptr), value, intent(in) :: sherwood
         integer(kind=c_int) :: init_sherwood_i
      end function init_sherwood_i

      function read_sherwood_dset_i(file_path, ptype, dset_name, dtype_id, buf) bind(C, name="read_sherwood_dset")
         use iso_c_binding
         use hdf5
         implicit none
         character(len=1, kind=c_char), dimension(*), intent(in) :: file_path
         integer(kind=c_int), value, intent(in) :: ptype
         character(len=1, kind=c_char), dimension(*), intent(in) :: dset_name
         integer(kind=hid_t), value, intent(in) :: dtype_id
         type(c_ptr), value, intent(in) :: buf
         integer(kind=c_int) :: read_sherwood_dset_i
      end function read_sherwood_dset_i
   end interface

contains
   subroutine init_sherwood_f(fpath, sherwood_o, status)
      implicit none
      character(len=*), intent(in) :: fpath
      type(sherwood_t), target :: sherwood_o
      integer, intent(inout), optional :: status

      type(c_ptr) :: sherwood_p

      integer :: h5err

      call H5open_f(h5err)

      if (h5err < 0) then
         print *, "[ERROR] Unable to initialize HDF5"
         stop
      end if

      sherwood_p = c_loc(sherwood_o)

      status = init_sherwood_i(trim(fpath)//c_null_char, sherwood_p)
   end subroutine init_sherwood_f

   subroutine read_sherwood_dset_1d_f(file_path, ptype, dset_name, dtype_id, buf, err)
      implicit none
      character(len=*), intent(in) :: file_path
      integer :: ptype
      character(len=*), intent(in) :: dset_name
      integer(hid_t), value, intent(in) :: dtype_id
      class(*), dimension(:), target :: buf
      integer :: err

      type(c_ptr) :: buf_p

      ! c_loc doesn't accept a polymorphic variable. Following is an attempt to
      ! fix this problem
      select type (buf)
      type is (integer(kind=4))
         buf_p = c_loc(buf(1))
      type is (integer(kind=8))
         buf_p = c_loc(buf(1))
      type is (real(kind=4))
         buf_p = c_loc(buf(1))
      type is (real(kind=8))
         buf_p = c_loc(buf(1))
      end select

      err = read_sherwood_dset_i(trim(file_path)//c_null_char, ptype, &
                                 trim(dset_name)//c_null_char, dtype_id, buf_p)
   end subroutine read_sherwood_dset_1d_f

   subroutine read_sherwood_dset_2d_f(file_path, ptype, dset_name, dtype_id, buf, err)
      implicit none
      character(len=*), intent(in) :: file_path
      integer :: ptype
      character(len=*), intent(in) :: dset_name
      integer(hid_t), value, intent(in) :: dtype_id
      class(*), dimension(:, :), target :: buf
      integer :: err

      type(c_ptr) :: buf_p

      ! c_loc doesn't accept a polymorphic variable. Following is an attempt to
      ! overcome this problem
      select type (buf)
      type is (integer(kind=4))
         buf_p = c_loc(buf(1, 1))
      type is (integer(kind=8))
         buf_p = c_loc(buf(1, 1))
      type is (real(kind=4))
         buf_p = c_loc(buf(1, 1))
      type is (real(kind=8))
         buf_p = c_loc(buf(1, 1))
      end select

      err = read_sherwood_dset_i(trim(file_path)//c_null_char, ptype, &
                                 trim(dset_name)//c_null_char, dtype_id, buf_p)
   end subroutine read_sherwood_dset_2d_f
end module libsherwood
