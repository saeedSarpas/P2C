/*
 * init_sherwood.c
 * tests_file: init_sherwood_tests.c
 *
 * Initialize the package by loading the snapshot general info (header,
 * constants, etc)
 *
 * @param: file_path: Formatted file path, e.g. /path/to/snap.%d.h5
 * @param: sherwood: An sherwood object to be filled
 *
 * @return: Standard EXIT_SUCCESS (EXIT_FAILURE on errors)
 */

#include <assert.h>
#include <high5/High5_types.h>
#include <high5/close_h5.h>
#include <high5/open_h5.h>
#include <high5/read_h5attrs.h>
#include <libsherwood/init_sherwood.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef PRINT
#define PRINT printf
#endif

hid_t init_sherwood(char *file_path_fmt, sherwood_t *sherwood) {
  if (strstr(file_path_fmt, "%d") == NULL) {
    printf("[Warning] Please use a formatted file path.\n");
    printf("          e.g. /path/to/sherwood/snap.%%d.h5\n");
    return EXIT_FAILURE;
  }

  char file_path[1024];
  sprintf(file_path, file_path_fmt, 0);

  hid_t file_id = open_h5(file_path, H5F_ACC_RDONLY, H5P_DEFAULT);
  int high5err, status = EXIT_SUCCESS;

  high5err = read_h5attrs(
      file_id, "Header", "BoxSize", H5T_NATIVE_DOUBLE,
      &(sherwood->header.BoxSize), optional_attr, "HubbleParam",
      H5T_NATIVE_DOUBLE, &(sherwood->header.HubbleParam), required_attr,
      "Omega0", H5T_NATIVE_DOUBLE, &(sherwood->header.Omega0), optional_attr,
      "OmegaLambda", H5T_NATIVE_DOUBLE, &(sherwood->header.OmegaLambda),
      optional_attr, "Redshift", H5T_NATIVE_DOUBLE,
      &(sherwood->header.Redshift), required_attr, "Time", H5T_NATIVE_DOUBLE,
      &(sherwood->header.Time), required_attr, "MassTable", H5T_NATIVE_DOUBLE,
      sherwood->header.MassTable, optional_attr, "Flag_Cooling", H5T_NATIVE_INT,
      &(sherwood->header.Flag_Cooling), optional_attr, "Flag_DoublePrecision",
      H5T_NATIVE_INT, &(sherwood->header.Flag_DoublePrecision), optional_attr,
      "Flag_Feedback", H5T_NATIVE_INT, &(sherwood->header.Flag_Feedback),
      optional_attr, "Flag_IC_Info", H5T_NATIVE_INT,
      &(sherwood->header.Flag_IC_Info), optional_attr, "Flag_Metals",
      H5T_NATIVE_INT, &(sherwood->header.Flag_Metals), optional_attr,
      "Flag_Sfr", H5T_NATIVE_INT, &(sherwood->header.Flag_Sfr), optional_attr,
      "Flag_StellarAge", H5T_NATIVE_INT, &(sherwood->header.Flag_StellarAge),
      optional_attr, "NumFilesPerSnapshot", H5T_NATIVE_INT,
      &(sherwood->header.NumFilesPerSnapshot), required_attr,
      "NumPart_ThisFile", H5T_NATIVE_INT, sherwood->header.NumPart_ThisFile,
      required_attr, "NumPart_Total", H5T_NATIVE_INT,
      sherwood->header.NumPart_Total, required_attr, "NumPart_Total_HighWord",
      H5T_NATIVE_INT, sherwood->header.NumPart_Total_HighWord, optional_attr,
      NULL);

  if (high5err == EXIT_FAILURE)
    status = EXIT_FAILURE;

  if (close_h5(file_id) == EXIT_FAILURE)
    return EXIT_FAILURE;

  return status;
}
