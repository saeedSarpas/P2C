logical function plotter_canvas_init_test() result(failed)
   use plotter

   implicit none

   type(plotter_canvas_t) :: canvas

   call canvas%init(32, 16)

   failed = .false.
end function plotter_canvas_init_test
