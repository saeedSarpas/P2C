module logging
   use, intrinsic :: iso_fortran_env, only: stdin => input_unit, stdout => output_unit, stderr => error_unit
   use plotter
   use string

   implicit none

   type, private :: logging_const_t
      integer :: closed = -10
      character(len=45) :: time_fmt = "(A,I4,A,I0.2,A,I0.2,A,I0.2,A,I0.2,A,I0.2,A)"
      character(len=7) :: default_logfile = "log.txt"
      character(len=7) :: default_errfile = "err.txt"
   end type logging_const_t

   type(logging_const_t), parameter :: logging_const = logging_const_t()

   type logging_t
      logical :: initialized = .false.
      character(len=1024) :: logfile = logging_const%default_logfile
      character(len=1024) :: errfile = logging_const%default_errfile
      character(len=1024), dimension(10) :: colored_logo = ''
      character(len=1024), dimension(10) :: logo = ''
      character(len=32) :: sections(32) = ''
      integer :: section_starts_at(32, 8) = 0
      integer :: secid = 0
      integer :: logfile_unit = logging_const%closed
      integer :: errfile_unit = logging_const%closed
      integer :: t(8)
   contains
      procedure :: init => logging_init
      procedure :: begin_section => logging_begin_section
      procedure :: end_section => logging_end_section

      procedure :: log => logging_log
      procedure :: warn => logging_warn
      procedure :: err => logging_err

      procedure :: update_time => logging_update_time
      procedure :: time => logging_time
      procedure :: time_and_section => logging_time_and_section
      procedure :: tas => logging_time_and_section

      procedure :: set_logo => logging_set_logo
      procedure :: set_colored_logo => logging_set_colored_logo

      procedure :: open_logfile => logging_open_logfile
      procedure :: close_logfile => logging_close_logfile
      procedure :: open_errfile => logging_open_errfile
      procedure :: close_errfile => logging_close_errfile

   end type logging_t

   interface
      module subroutine logging_log(this, message, key, ope, val)
         class(logging_t), intent(inout) :: this
         character(len=*), intent(in) :: message
         class(*), intent(in), optional :: key
         character(len=*), intent(in), optional :: ope
         class(*), intent(in), optional :: val(:)
      end subroutine logging_log

      module subroutine logging_warn(this, message, key, ope, val)
         class(logging_t), intent(inout) :: this
         character(len=*), intent(in) :: message
         class(*), intent(in), optional :: key
         character(len=*), intent(in), optional :: ope
         class(*), intent(in), optional :: val(:)
      end subroutine logging_warn

      module subroutine logging_err(this, message, key, ope, val)
         class(logging_t), intent(inout) :: this
         character(len=*), intent(in) :: message
         class(*), intent(in), optional :: key
         character(len=*), intent(in), optional :: ope
         class(*), intent(in), optional :: val(:)
      end subroutine logging_err

      module subroutine logging_update_time(this)
         class(logging_t), intent(inout) :: this
      end subroutine logging_update_time

      module function logging_time(this, color) result(time_str)
         class(logging_t), intent(inout) :: this
         character(len=*), intent(in), optional :: color
         character(len=64) :: time_str
      end function logging_time

      module function logging_time_and_section(this, color) result(tas_str)
         class(logging_t), intent(inout) :: this
         character(len=*), intent(in), optional :: color
         character(len=126) :: tas_str
      end function logging_time_and_section

      module subroutine logging_open_logfile(this)
         class(logging_t), intent(inout) :: this
      end subroutine logging_open_logfile

      module subroutine logging_close_logfile(this)
         class(logging_t), intent(inout) :: this
      end subroutine logging_close_logfile

      module subroutine logging_open_errfile(this)
         class(logging_t), intent(inout) :: this
      end subroutine logging_open_errfile

      module subroutine logging_close_errfile(this)
         class(logging_t), intent(inout) :: this
      end subroutine logging_close_errfile

      module subroutine logging_set_colored_logo(this)
         class(logging_t), intent(inout) :: this
      end subroutine logging_set_colored_logo

      module subroutine logging_set_logo(this)
         class(logging_t), intent(inout) :: this
      end subroutine logging_set_logo

      module subroutine logging_begin_section(this, section)
         class(logging_t), intent(inout) :: this
         class(*), intent(in) :: section
      end subroutine logging_begin_section

      module subroutine logging_end_section(this, print_duration)
         class(logging_t), intent(inout) :: this
         logical, intent(in), optional :: print_duration
      end subroutine logging_end_section
   end interface

contains

   subroutine logging_init(this, str)
      implicit none

      class(logging_t), intent(inout) :: this
      character(len=*), intent(in) :: str

      integer :: i

      if (this%initialized) return

      call this%update_time
      call this%set_logo
      call this%set_colored_logo

      write (this%logfile, fmt="(I4,A,I0.2,A,I0.2,A,I0.2,A,I0.2,A,I0.2,A,A)") &
         this%t(1), '-', this%t(2), '-', this%t(3), '-', this%t(5), '-', &
         this%t(6), '-', this%t(7), '-'//trim(str), '.log.txt'

      write (this%errfile, fmt="(I4,A,I0.2,A,I0.2,A,I0.2,A,I0.2,A,I0.2,A,A)") &
         this%t(1), '-', this%t(2), '-', this%t(3), '-', this%t(5), '-', &
         this%t(6), '-', this%t(7), '-'//trim(str), '.err.txt'

      ! Logo
      call this%open_logfile
      call this%open_errfile

      do i = 1, size(this%logo)
         write (stdout, *) trim(this%colored_logo(i))
         write (this%logfile_unit, *) trim(this%logo(i))
         write (this%errfile_unit, *) trim(this%logo(i))
      end do

      call this%close_logfile
      call this%close_errfile

      this%secid = 0

      this%initialized = .true.
   end subroutine logging_init

   function concat_components(msg, key, op, val, color) result(str)
      implicit none

      character(len=*), intent(in) :: msg, key, op, val
      character(len=*), intent(in), optional :: color
      character(len=2048) :: str

      if (present(color)) then
         str = trim(adjustl(msg))//' ' &
               //trim(adjustl(key))//' ' &
               //trim(adjustl(color))//trim(adjustl(op))//tc%nc//' ' &
               //trim(adjustl(val))
      else
         str = trim(adjustl(msg))//' ' &
               //trim(adjustl(key))//' ' &
               //trim(adjustl(op))//' ' &
               //trim(adjustl(val))
      end if

      str = trim(adjustl(str))
   end function concat_components
end module logging
