# Installation

## Dependencies

- [HDF5](https://www.hdfgroup.org/solutions/hdf5/):
  To build and install HDF5 library **locally**, follow these steps,
  - Download HDF5 tar file, `wget <path to HDF5 tar file>`[^1]
  - Untar it, `tar xvf hdf5-1.10.6.tar`
  - Change directory to `hdf5-1.10.6` and compile it,
  - `./configure --prefix=${HOME}/.local --enable-fortran && make && make install`
  - Make sure the compilation process is finished successfully

* In case you do not have a `.local` directory to store user compiled libraries and
  programs, follow these steps,
  - Create a `.local` directory in your home directory,
    ```bash
    mkdir -p ${HOME}/.local
    mkdir -p ${HOME}/.local/bin
    mkdir -p ${HOME}/.local/lib
    mkdir -p ${HOME}/.local/include
    ```
  - Add your `.local` directory to relevant env variables (you can copy/paste
    following lines into your shell session initialization file, i.e. `${HOME}/.profile`
    if you are using Bourne compatible shells like bash),
    ```bash
    # User's local directory
    export PATH=${HOME}/.local/bin:$PATH
    export INCLUDE_PATH=${HOME}/.local/include:$INCLUDE_PATH
    export C_INCLUDE_PATH=${HOME}/.local/include:$C_INCLUDE_PATH
    export LIBRARY_PATH=${HOME}/.local/lib:$LIBRARY_PATH
    export LD_LIBRARY_PATH=${HOME}/.local/lib:$LD_LIBRARY_PATH
    ```

## Compiling

- Clone this repository (or download its zip file).

  ```bash
  git clone https://gitlab.com/saeedSarpas/P2C.git --recursive
  ```

- If you already have cloned the repository and you want to update it,
  you can fetch the new version by running:

  ```bash
  git checkout master
  git pull origin master
  git submodule update --init --recursive
  ```

- Create a `build` directory,

  ```bash
  mkdir -p build
  ```

- Change directory into the `build` and generate a `Makefile`,

  ```bash
  cmake ..
  ```

- Finally build and compile the project by using the newly generated `Makefile`,
  ```bash
  make
  ```
  The executable file should be inside the `build` directory.

[^1]: Use this link to download HDF5 v1.10,

https://hdf-wordpress-1.s3.amazonaws.com/wp-content/uploads/manual/HDF5/HDF5_1_10_6/source/hdf5-1.10.6.tar
